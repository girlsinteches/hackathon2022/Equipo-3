from search_service.search_api import app as search_app


def run_app(host: str = 'localhost', port: str | int = 5003, debug: bool = False):
    search_app.run(
        host=host,
        port=int(port),
        debug=debug
    )
