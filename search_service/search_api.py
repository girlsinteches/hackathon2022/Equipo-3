from flask import Flask, jsonify

app = Flask(__name__)


@app.errorhandler(404)
def not_found(e):
    return jsonify({
        'status': 404,
        'error_key': 'page_not_found',
        'message': 'The page cannot been found'
    })


@app.errorhandler(500)
def internal_error(e):
    print(e)
    return jsonify({
        'status': 500,
        'error_key': 'internal_server_error',
        'message': 'Internal server error'
    })


@app.route('/')
def home():
    return jsonify({
        'name': 'Search API',
        'version': '0.0.01'
    })


if __name__ == '__main__':
    app.run()
