from extraction_service.extraction_api import app as extraction_app


def run_app(host: str = 'localhost', port: str | int = 5001, debug: bool = False):
    extraction_app.run(
        host=host,
        port=int(port),
        debug=debug
    )
