import json
from json import JSONDecodeError
from django.contrib.auth import authenticate
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.core.handlers.wsgi import WSGIRequest
from django.http import JsonResponse
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from core.models import Profile, UserSession


class BaseRestView(View):
    session = None

    def has_permission(self):
        if self.session:
            u = self.session.profile.user
            return u.is_staff | u.is_superuser
        return False

    def get_user_session(self) -> UserSession | None:
        token = self.request.headers.get('Authorization')
        if not token:
            return None
        try:
            return UserSession.objects.get(token=token)
        except ObjectDoesNotExist:
            return None
        except ValidationError:
            return None

    # TODO: think about returning UserSession object
    def validate_token(self) -> bool | None:
        token = self.request.headers.get('Authorization')
        if not token:
            return None
        try:
            # TODO: apply validation filter
            UserSession.objects.get(token=token)
            return True
        except ObjectDoesNotExist:
            return False
        except ValidationError:
            return None

    @classmethod
    def base_not_allowed_response(cls) -> JsonResponse:
        return JsonResponse({
            'status': 405,
            'message': 'Method not allowed'
        }, status=405)

    @classmethod
    def bad_request_error_response(cls, error_msg: str = 'Bad requesst') -> JsonResponse:
        return JsonResponse({
            'status': 400,
            'message': error_msg
        }, status=400)

    @classmethod
    def invalid_credentials_response(cls) -> JsonResponse:
        return JsonResponse({
            'status': 401,
            'message': 'Invalid credentials'
        }, status=401)

    @classmethod
    def not_found_response(cls, message: str = 'Page not found') -> JsonResponse:
        return JsonResponse({
            'status': 404,
            'message': message
        }, status=404)

    def parse_json_data(self) -> dict | None:
        try:
            return json.loads(self.request.body)
        except JSONDecodeError:
            # Log entry error
            return None

    def get_client_ip_and_agent(self) -> dict:
        x_forwarded_for = self.request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = self.request.META.get('REMOTE_ADDR')
        return {
            'user_address': ip,
            'user_agent': self.request.headers.get('User-Agent')
        }

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        # TODO: Deal with user sessions
        # TODO: Authorize user
        # TODO: Authenticate user
        return super(BaseRestView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        return self.base_not_allowed_response()

    def post(self, request, *args, **kwargs):
        return self.base_not_allowed_response()

    def put(self, request, *args, **kwargs):
        return self.base_not_allowed_response()

    def delete(self, request, *args, **kwargs):
        return self.base_not_allowed_response()

    def patch(self, request, *args, **kwargs):
        return self.base_not_allowed_response()


class LoginView(BaseRestView):

    def post(self, request: WSGIRequest, *args, **kwargs):
        user_login = self.parse_json_data()
        if not user_login:
            return self.bad_request_error_response(
                'Bad JSON Format'
            )
        if not user_login.get('user') or not user_login.get('auth'):
            return self.bad_request_error_response(
                'Bad request: user and auth params is required'
            )
        login = authenticate(
            username=user_login.get('user'),
            password=user_login.get('auth')
        )
        if not login:
            return self.invalid_credentials_response()
        user_profile = Profile.objects.get(user=login)
        session = UserSession(**{
            **{'profile': user_profile},
            **self.get_client_ip_and_agent()
        })
        session.save()
        return JsonResponse(session.serialize_dict(), status=201)


class CheckTokenView(BaseRestView):

    def get(self, request, *args, **kwargs):
        validation_status = self.validate_token()
        status_code = 200 if validation_status else 401
        return JsonResponse({
            'status': status_code,
            'token_status': validation_status
        }, status=status_code)
