from django.contrib import admin

# Register your models here.
from core.models import Profile, UserSession, SafeContact, NotificationMethod, SafeContactNotificationMethods

admin.site.register(Profile)
admin.site.register(UserSession)
admin.site.register(SafeContact)
admin.site.register(NotificationMethod)
admin.site.register(SafeContactNotificationMethods)