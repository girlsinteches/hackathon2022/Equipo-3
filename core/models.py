import datetime
import uuid

from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.utils.text import slugify


class BaseModelManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(alive=True)


class BaseModel(models.Model):
    id = models.UUIDField(
        primary_key=True,
        auto_created=True,
        default=uuid.uuid4().hex,
        verbose_name='ID'
    )
    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name='Created at'
    )
    updated_at = models.DateTimeField(
        verbose_name='Updated at'
    )
    deleted_at = models.DateTimeField(
        null=True,
        blank=True,
        verbose_name='Deleted at'
    )
    alive = models.BooleanField(
        default=True,
        verbose_name='Alive'
    )

    objects = BaseModelManager()
    all_objects = models.Manager()

    def __str__(self):
        return f'ID: {self.id}'

    def save(
        self, force_insert=False, force_update=False, using=None, update_fields=None
    ):
        now = datetime.datetime.now()
        if not self.created_at:
            self.created_at = now
        self.updated_at = now
        super(BaseModel, self).save(
            force_insert=force_insert,
            force_update=force_update,
            using=using,
            update_fields=update_fields
        )

    def serialize_dict(self):
        return {
            'id': self.id,
            'created_at': self.created_at.isoformat(),
            'updated_at': self.updated_at.isoformat(),
            'deleted_at': self.deleted_at.isoformat() if self.deleted_at else None,
            'alive': self.alive
        }

    def delete(self, hard_delete: bool = False, using=None, keep_parents=False):
        if hard_delete:
            super(BaseModel, self).delete(using=using, keep_parents=keep_parents)
        else:
            self.deleted_at = datetime.datetime.now()
            self.alive = False
            self.save()

    class Meta:
        abstract = True


class Profile(BaseModel):
    user = models.OneToOneField(
        User,
        verbose_name='User',
        related_query_name='profiles_user',
        on_delete=models.CASCADE
    )

    def __str__(self):
        return f'{self.user.username}: {self.id}'

    def serialize_dict(self):
        return {**super(Profile, self).serialize_dict(), **{
            'username': self.user.username,
            'is_staff': self.user.is_staff,
            'is_admin': self.user.is_superuser,
            'id': self.id
        }}

    class Meta:
        verbose_name = 'Profile'
        verbose_name_plural = 'Profiles'


class UserSession(BaseModel):
    token = models.UUIDField()
    profile = models.ForeignKey(
        Profile,
        related_query_name='user_sessions_profile',
        on_delete=models.CASCADE
    )
    user_agent = models.CharField(
        max_length=2048,
        null=True,
        blank=True
    )
    user_address = models.CharField(
        max_length=128,
        null=True,
        blank=True
    )
    valid_until = models.DateTimeField(
        null=True,
        blank=True
    )

    def __str__(self):
        return f'Session: {self.token} of {self.profile.user.username}'

    def save(
        self, force_insert=False, force_update=False, using=None, update_fields=None
    ):
        self.token = uuid.uuid4().hex
        super(UserSession, self).save(
            force_insert=force_insert,
            force_update=force_update,
            using=using,
            update_fields=update_fields
        )

    def serialize_dict(self):
        return {**super(UserSession, self).serialize_dict(), **{
            'token': self.token,
            'user_agent': self.user_agent,
            'user_address': self.user_address,
            'valid_until': self.valid_until
        }}

    class Meta:
        verbose_name = 'User Session'
        verbose_name_plural = 'User Sessions'


class SafeContact(BaseModel):
    profile = models.ForeignKey(
        Profile,
        verbose_name='Profile',
        related_query_name='safe_contacts_profile',
        on_delete=models.CASCADE
    )
    name = models.CharField(
        max_length=1024,
        verbose_name='Name'
    )
    nickname = models.CharField(
        max_length=1024,
        verbose_name='Nickname'
    )
    email = models.EmailField(
        null=True,
        blank=True
    )
    # TODO: validate phone number
    phone = models.CharField(
        max_length=64,
        null=True,
        blank=True
    )

    def __str__(self):
        return f'Safe contact {self.name} of {self.profile.user.username}'

    def serialize_dict(self):
        return {**super(SafeContact, self), **{
            'profile': self.profile.serialize_dict(),
            'name': self.name,
            'nickname': self.nickname,
            'email': self.email,
            'phone': self.phone
        }}

    class Meta:
        verbose_name = 'Safe Contact'
        verbose_name_plural = 'Safe Contacts'


class NotificationMethod(BaseModel):
    name = models.CharField(
        max_length=512,
        verbose_name='Name',
        unique=True
    )
    slug = models.SlugField(
        max_length=724,
        verbose_name='Slug',
        unique=True
    )

    def __str__(self):
        return f'Notification method: {self.name}'

    def save(
        self, force_insert=False, force_update=False, using=None, update_fields=None
    ):
        self.slug = slugify(self.name)
        super(NotificationMethod, self).save(
            force_insert=force_insert,
            force_update=force_update,
            using=using,
            update_fields=update_fields
        )

    def serialize_dict(self):
        return {**super(NotificationMethod, self).serialize_dict(), **{
            'name': self.name,
            'slug': self.slug
        }}

    class Meta:
        verbose_name = 'Notification Method'
        verbose_name_plural = 'Notification Methods'


class SafeContactNotificationMethods(BaseModel):
    contact = models.ForeignKey(
        SafeContact,
        verbose_name='Safe contact',
        related_query_name='safe_contact_notification_methods_contact',
        on_delete=models.CASCADE
    )

    notification = models.ForeignKey(
        NotificationMethod,
        verbose_name='Notification Method',
        related_query_name='safe_contact_notification_methods_notification',
        on_delete=models.CASCADE
    )

    def __str__(self):
        return f'{self.contact.profile.user.username}, contact {self.contact.name}, method: {self.notification.name}'

    def serialize_dict(self):
        return {**super(SafeContactNotificationMethods, self).serialize_dict(), **{
            'contact': self.contact.serialize_dict(),
            'notification': self.notification.serialize_dict()
        }}

    class Meta:
        verbose_name = 'Safe Contact Notification Method'
        verbose_name_plural = 'Safe Contact Notification Methods'
