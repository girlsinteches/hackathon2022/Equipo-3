# Generated by Django 4.1 on 2022-09-01 23:37

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='NotificationMethod',
            fields=[
                ('id', models.UUIDField(auto_created=True, default='6f75bc2f8566424bac4e94ea4768553b', primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('updated_at', models.DateTimeField(verbose_name='Updated at')),
                ('deleted_at', models.DateTimeField(blank=True, null=True, verbose_name='Deleted at')),
                ('alive', models.BooleanField(default=True, verbose_name='Alive')),
                ('name', models.CharField(max_length=512, unique=True, verbose_name='Name')),
                ('slug', models.SlugField(max_length=724, unique=True, verbose_name='Slug')),
            ],
            options={
                'verbose_name': 'Notification Method',
                'verbose_name_plural': 'Notification Methods',
            },
        ),
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.UUIDField(auto_created=True, default='6f75bc2f8566424bac4e94ea4768553b', primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('updated_at', models.DateTimeField(verbose_name='Updated at')),
                ('deleted_at', models.DateTimeField(blank=True, null=True, verbose_name='Deleted at')),
                ('alive', models.BooleanField(default=True, verbose_name='Alive')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_query_name='profiles_user', to=settings.AUTH_USER_MODEL, verbose_name='User')),
            ],
            options={
                'verbose_name': 'Profile',
                'verbose_name_plural': 'Profiles',
            },
        ),
        migrations.CreateModel(
            name='SafeContact',
            fields=[
                ('id', models.UUIDField(auto_created=True, default='6f75bc2f8566424bac4e94ea4768553b', primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('updated_at', models.DateTimeField(verbose_name='Updated at')),
                ('deleted_at', models.DateTimeField(blank=True, null=True, verbose_name='Deleted at')),
                ('alive', models.BooleanField(default=True, verbose_name='Alive')),
                ('name', models.CharField(max_length=1024, verbose_name='Name')),
                ('nickname', models.CharField(max_length=1024, verbose_name='Nickname')),
                ('email', models.EmailField(blank=True, max_length=254, null=True)),
                ('phone', models.CharField(blank=True, max_length=64, null=True)),
                ('profile', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_query_name='safe_contacts_profile', to='core.profile', verbose_name='Profile')),
            ],
            options={
                'verbose_name': 'Safe Contact',
                'verbose_name_plural': 'Safe Contacts',
            },
        ),
        migrations.CreateModel(
            name='UserSession',
            fields=[
                ('id', models.UUIDField(auto_created=True, default='6f75bc2f8566424bac4e94ea4768553b', primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('updated_at', models.DateTimeField(verbose_name='Updated at')),
                ('deleted_at', models.DateTimeField(blank=True, null=True, verbose_name='Deleted at')),
                ('alive', models.BooleanField(default=True, verbose_name='Alive')),
                ('token', models.UUIDField()),
                ('user_agent', models.CharField(blank=True, max_length=2048, null=True)),
                ('user_address', models.CharField(blank=True, max_length=128, null=True)),
                ('valid_until', models.DateTimeField(blank=True, null=True)),
                ('profile', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_query_name='user_sessions_profile', to='core.profile')),
            ],
            options={
                'verbose_name': 'User Session',
                'verbose_name_plural': 'User Sessions',
            },
        ),
        migrations.CreateModel(
            name='SafeContactNotificationMethods',
            fields=[
                ('id', models.UUIDField(auto_created=True, default='6f75bc2f8566424bac4e94ea4768553b', primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created at')),
                ('updated_at', models.DateTimeField(verbose_name='Updated at')),
                ('deleted_at', models.DateTimeField(blank=True, null=True, verbose_name='Deleted at')),
                ('alive', models.BooleanField(default=True, verbose_name='Alive')),
                ('contact', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_query_name='safe_contact_notification_methods_contact', to='core.safecontact', verbose_name='Safe contact')),
                ('notification', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_query_name='safe_contact_notification_methods_notification', to='core.notificationmethod', verbose_name='Notification Method')),
            ],
            options={
                'verbose_name': 'Safe Contact Notification Method',
                'verbose_name_plural': 'Safe Contact Notification Methods',
            },
        ),
    ]
