from nlp_service.nlp_api import app as nlp_app


def run_app(host: str = 'localhost', port: str | int = 5002, debug: bool = False):
    nlp_app.run(
        host=host,
        port=int(port),
        debug=debug
    )
