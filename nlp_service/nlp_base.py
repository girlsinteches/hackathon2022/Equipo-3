import spacy
from spacy import Language

valid_languages = {
    'es': 'es_core_news_sm',
    'en': 'en_core_web_sm'
}


def get_spacy_lang(lang: str) -> Language | None:
    spacy_lang = valid_languages.get(lang)
    if not spacy_lang:
        return None
    return spacy.load(lang)


def get_tokens_dict(spacy_instance: Language, text_input: str):
    found_tokens = []
    tokens = spacy_instance(text_input)
    for token in tokens:
        found_tokens.append(
            {
                "text": token.text,  # Original word text
                "lemma": token.lemma_,  # The Lemmatized text
                "pos": token.pos_,  # Simple part-of-speech tag
                "tag": token.tag_,  # Detailed part-of-speech tag
                "dep": token.dep_,  # Syntactic dependency, i.e.: relation between tokens
                "shape": token.shape_,  # Word shape
                "alpha": token.is_alpha,  # Is a alpha character
                "stop": token.is_stop,  # Is a stopword
                "head_text": token.head.text if token.head else None,  # The head text
                "head_pos": token.head.pos_ if token.head else None,  # The head part-of-speech tag
                # "childs": [child for child in token.children] # Dependants
            }
        )
    return found_tokens
