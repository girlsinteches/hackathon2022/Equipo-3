"""besafe URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

import besafe_alerts.views
import core.views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('login', core.views.LoginView.as_view(), name='login'),
    path('validate_token', core.views.CheckTokenView.as_view(), name='check_token'),
    path('besafe/alert', besafe_alerts.views.AlertView.as_view(), name='alert'),
    path('besafe/alert/<str:alert_id>', besafe_alerts.views.AlertView.as_view(), name='alert'),
    path('besafe/alerts', besafe_alerts.views.AlertListView.as_view(), name='alerts')
]
