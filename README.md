# Besafe Project

Evolution of H4H Hackathon Project for safety 
traveling in your day basics

## IMPORTANT
New project has been already defined, it will 
take time until all pieces work together, the 
legacy code will be in old_project folder

## Technologies

- Python (Backend side services and data analysis)
- JavaScript (Frontend side)
- Meilisearch (Search Engine)
- Neo4J (Graph database engine)
- PostgreSQL / SQLite (SQL database engine)
- Redis (Caching / Task queue)
- Flutter (For mobile application side)