from neo4j import Driver, GraphDatabase, basic_auth, Session


class GraphInterface(object):
    driver: Driver = None

    def __init__(self, host: str, port: str | int, user: str, auth: str):
        self.driver = self.init_driver(
            host=host,
            port=port,
            user=user,
            auth=auth
        )

    @classmethod
    def init_driver(cls, host: str, port: str | int, user: str, auth: str) -> Driver:
        return GraphDatabase.driver(
            f'bolt://{host}:{port}',
            auth=basic_auth(
                user=user,
                password=auth
            )
        )

    @classmethod
    def get_session(cls, driver: Driver) -> Session:
        return driver.session()
