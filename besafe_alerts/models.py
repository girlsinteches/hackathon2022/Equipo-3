import datetime

from django.db import models

# Create your models here.
from django.utils.text import slugify

from core.models import BaseModel, Profile


class AlertCategory(BaseModel):
    name = models.CharField(
        max_length=512,
        verbose_name='Alert Category',
        unique=True
    )
    slug = models.SlugField(
        max_length=724,
        verbose_name='Slug category',
        unique=True
    )

    def __str__(self):
        return f'Category: {self.name}'

    def save(
        self, force_insert=False, force_update=False, using=None, update_fields=None
    ):
        self.slug = slugify(self.name)
        super(AlertCategory, self).save(
            force_insert=force_insert,
            force_update=force_update,
            using=using,
            update_fields=update_fields
        )

    def serialize_dict(self):
        return {**super(AlertCategory, self).serialize_dict(), **{
            'name': self.name,
            'slug': self.slug
        }}

    class Meta:
        verbose_name = 'Alert category'
        verbose_name_plural = 'Alert categories'


class BesafeAlert(BaseModel):
    category = models.ForeignKey(
        AlertCategory,
        verbose_name='Alert Category',
        related_query_name='besafe_alerts_category',
        on_delete=models.CASCADE,
        null=True
    )
    profile = models.ForeignKey(
        Profile,
        verbose_name='Profile',
        related_query_name='besafe_alerts_profile',
        on_delete=models.CASCADE
    )

    lat = models.FloatField(
        verbose_name='Latitude'
    )
    lon = models.FloatField(
        verbose_name='Longitude'
    )

    description = models.TextField(
        verbose_name='Description',
        null=True,
        blank=True
    )

    def __str__(self):
        return f'Alert {self.id} of {self.profile.user.username}'

    def serialize_dict(self, safe_request: bool = False):
        return {**super(BesafeAlert, self).serialize_dict(), **{
            'category': self.category.serialize_dict() if self.category else None,
            'profile': None if safe_request else self.profile.serialize_dict(),
            'lat': self.lat,
            'lon': self.lon,
            'description': self.description
        }}

    def get_serialized_point(self):
        return {
            'id': self.id,
            'lat': self.lat,
            'lon': self.lon
        }

    def update(self, category_id: str = None, description: str = None, force_update: bool = False):
        update = force_update

        if self.category and category_id is None:
            self.category = None
        if self.description and description is None:
            self.description = None
        if category_id:
            self.category_id = category_id
        if description:
            self.description = description

        if update:
            self.save()

    class Meta:
        verbose_name = 'Besafe Alert'
        verbose_name_plural = 'Besafe Alerts'
