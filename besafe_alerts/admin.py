from django.contrib import admin

# Register your models here.
from besafe_alerts.models import AlertCategory, BesafeAlert

admin.site.register(AlertCategory)
admin.site.register(BesafeAlert)