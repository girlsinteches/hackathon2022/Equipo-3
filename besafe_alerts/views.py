from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView
from besafe_alerts.models import BesafeAlert
from core.views import BaseRestView


class AlertView(BaseRestView):
    model = BesafeAlert

    # TODO: Think about moving session validation to parent class
    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        session = self.get_user_session()
        if not session and request.method != 'GET':
            return self.invalid_credentials_response()
        self.session = session
        return super(AlertView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        input_json = self.parse_json_data()
        new_alert = self.model(
            profile=self.session.profile,
            lat=float(input_json.get('lat')),
            lon=float(input_json.get('lon')),
            category_id=input_json.get('category_id'),
            description=input_json.get('description')
        )
        new_alert.save()
        return JsonResponse(
            new_alert.serialize_dict(),
            status=201
        )

    def get(self, request, *args, **kwargs):
        try:
            if self.session and self.session.profile.user.is_staff:
                besafe_alert = self.model.all_objects.get(id=kwargs.get('alert_id'))
            else:
                besafe_alert = self.model.objects.get(id=kwargs.get('alert_id'))
            return JsonResponse(besafe_alert.serialize_dict(safe_request=self.session is None))
        except ObjectDoesNotExist:
            return self.not_found_response(
                'Alert not found'
            )

    def put(self, request, *args, **kwargs):
        input_json = self.parse_json_data()
        if not input_json:
            return self.bad_request_error_response(
                'Bad request: no JSON data has been provided'
            )
        if not input_json.get('category_id') and not input_json.get('description'):
            return self.bad_request_error_response(
                'Bad request: category_id or description attributes should be provided'
            )
        try:
            besafe_alert = self.model.objects.get(
                id=kwargs.get('alert_id'),
                profile_id=self.session.profile_id
            )
            besafe_alert.update(
                category_id=input_json.get('category_id'),
                description=input_json.get('description')
            )

            return JsonResponse(besafe_alert.serialize_dict(safe_request=self.session is None))
        except ObjectDoesNotExist:
            return self.not_found_response(
                'Alert not found'
            )

    def delete(self, request, *args, **kwargs):
        try:
            besafe_alert = self.model.objects.get(
                id=kwargs.get('alert_id'),
                profile_id=self.session.profile_id
            )
            besafe_alert.delete()
            return JsonResponse(besafe_alert.serialize_dict(safe_request=self.session is None))
        except ObjectDoesNotExist:
            return self.not_found_response(
                'Alert not found'
            )


class AlertListView(ListView, BaseRestView):
    model = BesafeAlert
    context_object_name = 'besafe_alerts'
    paginate_by = 100

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        session = self.get_user_session()
        if not session and request.method != 'GET':
            return self.invalid_credentials_response()
        self.session = session
        return super(AlertListView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        alerts = self.get_queryset()
        # TODO: Deal with pagination params
        return JsonResponse({
            'alerts': [a.serialize_dict(
                safe_request=not self.has_permission()
            ) for a in alerts]
        })
