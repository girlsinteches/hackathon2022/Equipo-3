from django.apps import AppConfig


class BesafeAlertsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'besafe_alerts'
