#!/bin/bash

echo "Deploying Frontend in Apache: "
echo "Installing Node Dependencies"
npm install
echo "Making a React build"
npm run build
echo "Copying the build"
sudo rm -rf /var/www/html/static
sudo cp -r build/* /var/www/html/
echo "Changing permissions"
sudo chown -R www-data:www-data /var/www/html
echo "Deployed"


exit 0