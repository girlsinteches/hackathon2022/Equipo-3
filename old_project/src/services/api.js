

class Api{
    base_url_dev = 'http://localhost:5001';
    base_url_pro = 'http://besafe.jasonjimnzdev.es:5001';
    //base_url = 'http://localhost:5001';
    base_url = 'http://besafe.jasonjimnzdev.es:5001';
    env = null;

    constructor(env) {
        if (env){
            this.env = env;
        } else {
            this.env = 'dev';
        }
    }

    callApi(path, method, data, userToken){
        if (method.toLowerCase() === 'get'){
            return fetch(
                `${this.base_url}${path}`, {
                    method: method,
                    headers: {
                        'Content-Type': 'application/json',
                        'user_token': userToken
                    },
                    mode: 'cors'
                }
            )
        }
        return fetch(
            `${this.base_url}${path}`, {
                method: method,
                headers: {
                    'Content-Type': 'application/json',
                    'user_token': userToken
                },
                mode: 'cors',
                body: JSON.stringify(data)
            }
        )
    }
}

export default Api;