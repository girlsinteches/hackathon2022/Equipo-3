import React, {useState} from "react";
import {Circle, MapContainer, Marker, Popup, TileLayer, useMap} from 'react-leaflet'
import './mapComponent.scss';
import Api from "../../services/api";

const MapUpdater = ({...props}) => {
    const vmap = useMap();
    navigator.geolocation.getCurrentPosition((pos) => {
        const crd = pos.coords;
        vmap.setView([crd.latitude, crd.longitude], 17)
    });
    vmap.setView(props.center, props.zoom)
    return null;
}
const MapComponent = ({mapLat, mapLon, ...props}) => {
    // [51.505, -0.09]
    const position = [mapLat, mapLon];
    const [positions, setPositions] = useState([])
    const [positionsLoaded, setPositionsLoaded] = useState(false);
    if (!positionsLoaded) {
        setPositionsLoaded(true);
        const api = new Api('dev');
        api.callApi('/api/alerts', 'get', {}, null).then((response) => {
            response.json().then((r) => {
                const positionApiList = r.map((d) => {
                    const obj = {
                        coords: [d.latitude, d.longitude],
                        alert_id: d.alert_id,
                        alert_occurred: d.created_at,
                        category: d.category
                    }
                    return obj
                });
                setPositions(positionApiList);
            })
        })
    }
    return <div>
        <MapContainer center={position} zoom={13} scrollWheelZoom={true} className={"mapContainer"}>
            <MapUpdater center={position} zoom={13}/>
            <TileLayer
                attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            {positions.map((p) => {
                    return <Marker position={p.coords}>
                        <Popup>Alerta ocurrida
                            <ul>
                                <li>Tipo: {p.category}</li>
                                <li>Identificada a la siguiente hora: {new Date(p.alert_occurred).toTimeString()}</li>
                                <li>Con identificador: {p.alert_id}</li>
                            </ul>
                        </Popup>
                        <Circle center={p.coords} radius={250}/>
                    </Marker>
                }
            )}


        </MapContainer>
    </div>
}

export default MapComponent;