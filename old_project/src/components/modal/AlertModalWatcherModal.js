import React, {useState} from "react";

const alertTypes = [
    {name: 'Agresión verbal'},
    {name: 'Agresión física'},
    {name: 'Acoso'},
    {name: 'Accidente'}
]

const AlertModalWatcherModal = ({...props}) => {
    return <div className="modalBackdrop">
        <div className="modal">
            <div className="modalHeader">
                <h4>Crear alerta</h4>
                <span className="closeModal" onClick={(e) =>{
                    window.location.href = '/landing';
                }}>&times;</span>
            </div>
            <div className="modalBody">
                <div className="buttonList">
                    {alertTypes.map((element) => (
                            <button>{element.name}</button>
                        ))}
                </div>
            </div>
            <div className="modalFooter">
                <div className="form-row">
                    <button onClick={(e) => {
                        e.preventDefault();
                        window.location.href = '/landing';
                    }}>Cerrar</button>
                </div>
            </div>
        </div>
    </div>
}

export default AlertModalWatcherModal;