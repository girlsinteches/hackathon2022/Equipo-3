import React, {useState} from "react";
import './modal.scss';
import Api from "../../services/api";

const LoginModal = ({modalCallback, ...props}) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [loginError, setLoginError] = useState(false);
    if (window.localStorage.getItem('session_token'))
        window.location.href = '/landing';
    function loginAction(event){
        setLoginError(false);
        event.preventDefault();
        const api = new Api('dev');
        api.callApi('/api/login', 'post', {
            'email': email,
            'password': password
        }, null).then((r) => {
            r.json().then((json_response) => {
                if (json_response.err){
                    setLoginError(true);
                }
                if (json_response.token && json_response.user_uuid){
                    window.localStorage.setItem('session_token', json_response.token)
                    window.localStorage.setItem('user_id', json_response.user_uuid)
                    window.location.href = '/landing';
                }
            });
        }).catch((error) => {
            console.log("Error login");
        })
    }
    function manageLoginFields(event){
        const inputName = event.target.name;
        const inputValue = event.target.value;
        switch (inputName){
            case 'email':
                setEmail(inputValue);
                break;
            case 'password':
                setPassword(inputValue);
                break;
            default:
                break;
        }
    }
    return <div className="modalBackdrop">
        <div className="modal">
            <div className="modalHeader">
                <h4>Login</h4>
                <span className="closeModal" onClick={(e) =>{
                    window.location.href = '/';
                }}>&times;</span>
            </div>
            <div className="modalBody">
                <form className={"loginModalForm"}>
                    <div className="form-row">
                      <label>Email:</label>
                        <input
                            type="email"
                            name={"email"}
                            placeholder={"Tu correo electrónico"}
                            onChange={manageLoginFields}
                            value={email}
                        />
                    </div>
                    <div className="form-row">
                        <label>Contraseña:</label>
                        <input
                            type="password"
                            name={"password"}
                            placeholder={"Contraseña"}
                            onChange={manageLoginFields}
                            value={password}
                        />
                    </div>
                </form>
                {loginError ? <div><h3>Las credenciales introducidas no son correctas</h3></div> : null}
            </div>
            <div className="modalFooter">
                <div className="form-row">
                    <button onClick={(e) =>{
                        window.location.href = '/';
                    }}>Cerrar</button>
                    <button className={"loginSubmitButton"} onClick={loginAction}>Iniciar sesión</button>
                </div>
            </div>
        </div>
    </div>
}

export default LoginModal;