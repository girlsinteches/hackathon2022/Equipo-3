import React, {useState} from "react";

const AlertModal = ({...props}) => {
    return <div className="modalBackdrop">
        <div className="modal">
            <div className="modalHeader">
                <h4>Crear alerta</h4>
                <span className="closeModal" onClick={(e) =>{
                    window.location.href = '/landing';
                }}>&times;</span>
            </div>
            <div className="modalBody">
                <div className="buttonList">
                    <div className="bigButton">
                        <button onClick={(e) => {
                            window.location.href = '/alert_watcher';
                        }}>
                            <h2>Lo estoy observando</h2>
                        </button>
                    </div>
                    <div className="bigButton">
                        <button>
                            <h2>Me está pasando a mí</h2>
                        </button>
                    </div>
                </div>
            </div>
            <div className="modalFooter">
                <div className="form-row">
                    <button onClick={(e) => {
                        e.preventDefault();
                        window.location.href = '/landing';
                    }}>Cerrar</button>
                </div>
            </div>
        </div>
    </div>
}

export default AlertModal;