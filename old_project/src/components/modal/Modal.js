import React, {useState} from "react";

const Modal = ({modalHeader, modalBody, modalFooter, ...props}) => {
    return <div className="modalBackdrop">
        <div className="modal">
            <div className="modalHeader">
                {modalHeader}
            </div>
            <div className="modalBody">
                {modalBody}
            </div>
            <div className="modalFooter">
                {modalFooter}
            </div>
        </div>
    </div>
}

export default Modal;