import React, {useState} from "react";
import Api from "../../services/api";
import './register.scss';

const Register = ({...props}) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [nickname, setNickname] = useState('')
    const [validationErrors, setValidationErrors] = useState(false);
    const [userExists, setUserExists] = useState(false);
    function registerAction(event){
        setValidationErrors(false);
        event.preventDefault();
        if (!email || !password || !confirmPassword || !nickname || password !== confirmPassword){
            setValidationErrors(true);
        } else {
            const api = new Api('dev');
            api.callApi('/api/user', 'post', {
                "email": email,
                "nickname": nickname,
                "password": password
            }, null).then((response) => {
                response.json().then((r) => {
                    if (r.err){
                        console.log("Usuario existe")
                        setUserExists(true)
                    } else {
                        window.location.href = "/landing";
                    }
                })
            })

        }
    }

    function manageInputs(event){
        const name = event.target.name;
        const value = event.target.value;
        switch (name){
            case 'email':
                setEmail(value);
                break;
            case 'password':
                setPassword(value);
                break;
            case 'confirmPassword':
                setConfirmPassword(value);
                break;
            case 'nickname':
                setNickname(value);
                break;
            default:
                break;
        }
    }
    return <div className={"registerView"}>
        <h3>Registro</h3>
        <form onSubmit={registerAction}>
            <div className="form-row">
                <label htmlFor="email">Email: </label>
                <input
                    type="email"
                    name={"email"}
                    placeholder={"Email"}
                    onChange={manageInputs}
                    value={email}
                />
            </div>
            <div className="form-row">
                <label htmlFor="nickname">Nick: </label>
                <input
                    type="text"
                    name={"nickname"}
                    placeholder={"Nick"}
                    onChange={manageInputs}
                    value={nickname}
                />
            </div>
            <div className="form-row">
                <label htmlFor="password">Contraseña: </label>
                <input
                    type="password"
                    name={"password"}
                    placeholder={"Contraseña"}
                    onChange={manageInputs}
                    value={password}
                />
            </div>
            <div className="form-row">
                <label htmlFor="confirmPassword">Confirmar contraseña: </label>
                <input
                    type="password"
                    name={"confirmPassword"}
                    placeholder={"Confirmar contraseña"}
                    onChange={manageInputs}
                    value={confirmPassword}
                />
            </div>
            <div className="form-row">
                <label htmlFor="firstName">Nombre: </label>
                <input type="text" name={"firstName"} placeholder={"Nombre"}/>
            </div>
            <div className="form-row">
                <label htmlFor="lastName">Apellidos: </label>
                <input type="text" name={"lastName"} placeholder={"Apellidos"} />
            </div>
            <div className="form-row">
                <label htmlFor="age">Edad: </label>
                <input type="number" min={0} name={"age"} placeholder={"Edad"}/>
            </div>
            <div className="form-row">
                <label htmlFor="gender">Género: </label>
                <select name="gender" id="genderSelect">
                    <option>Selecciona una opción</option>
                    <option value="male">Masculino</option>
                    <option value="female">Femenino</option>
                    <option value="agender">Agénero</option>
                    <option value="nb">No binario</option>
                    <option value="other">Otro</option>
                </select>
            </div>
            <div className="form-row">
                <label htmlFor="collective">Colectivo: </label>
                <input type="text" name={"collective"} placeholder={"Colectivo"}/>
            </div>
            {validationErrors ? <div className="form-row-full">
                <h3 style={{color: "red"}}>Hubo un error en el formulario, asegúrate de rellenar email, nickname, contraseña y que la contraseña y verificación coincidan</h3>
            </div> : null}
            {userExists ? <div className="form-row-full">
                <h3 style={{color: "red"}}>El nick o email ya están registrados en base de datos</h3>
            </div> : null}
            <p style={{width: '90%', margin: '1rem auto'}}><b>
                DISCLAIMER:Esta aplicación está en modo
                de pruebas, no uses correos originales
                o contraseñas reales puesto que se
                guardan en base de datos y la aplicación
                no cuenta de momento con certificado SSL
                para proteger como viajan los datos.<br/><br/>
                El nombre, apellidos, edad, género y colectivo,
                son campos que no salen del formulario,
                puedes rellenarlos libremente pero no harán
                nada ni se envíaran ;) <br/><br/>
                El email y el nick han de ser únicos pero pueden
                ser perfectamente inventados, es más
                recomendamos encarecidamente que no sean
                reales, no hay envío de correos, las
                contraseñas pueden ser perfectamente
                genéricas, la geoposición se asociará
                al usuario creado para testear la
                funcionalidad de la app, y se aplicará
                una modificación aleatoria de los puntos
                de esta forma las geoposiciones no serán
                totalmente exactas a la hora de generar
                una alerta, por tanto ten en cuenta
                este texto antes de pulsar el botón
                de registro.
            </b></p>
            <div className="form-row-full">
                <button type={"submit"}>Registrar usuario</button>
            </div>
        </form>
    </div>
}

export default Register;