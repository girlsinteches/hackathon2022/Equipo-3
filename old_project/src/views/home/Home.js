import React, {useState} from "react";
import MapComponent from "../../components/map/MapComponent";
import './home.scss';

const Home = ({...props}) => {
    // Lat 40.4165 Lon -3.70256 -- Madrid
    return <div>
        <div className="actionButtons">
            <button className="loginButton" onClick={(e) => {window.location.href ='/login'}}>Login</button>
            <button className="registerButton" onClick={(e) => {
            window.location.href="/register";}
            }>Registro</button>
        </div>
        <MapComponent mapLat={40.4165} mapLon={-3.70256} />
    </div>
}

export default Home;