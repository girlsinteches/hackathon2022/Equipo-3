import React, {useState} from "react";
import MapComponent from "../../components/map/MapComponent";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import '../home/home.scss';
import {faSearch} from "@fortawesome/free-solid-svg-icons";
import Api from "../../services/api";

const Landing = ({...props}) => {
    if (!window.localStorage.getItem('session_token'))
        window.location.href = "/";

    function alertAction(event) {
        event.preventDefault();
        const token = window.localStorage.getItem('session_token');
        if (token){
            const api = new Api('dev');
            navigator.geolocation.getCurrentPosition((pos) => {
                const crd = pos.coords
                //TODO: Applied randomizer for geoposition store
                api.callApi('/api/alert', 'post', {
                    latitude: crd.latitude + (Math.random() * (100 - 1)) / 500,
                    longitude: crd.longitude + (Math.random() * (100 - 1)) / 1000
                }, token).then((response) => {
                    response.json().then((r) => {
                        api.callApi(`/api/alert/${r.alert.alert_id}`, 'put', {
                            category: 'aggresion'
                        }, token).then((response2) => {
                            response2.json().then((r2) => {
                                window.location.href = '/alert';
                            })
                        })
                    })
                })
            }, (error) => {
                console.log(error);
                window.alert("Es necesaria la geoposición para lanzar una alerta");
            });
        }
    }
    return <div>
        <div className="searchBar">
            <div className="barContainer">
                <input type="text" placeholder={"Introduce tu destino"}/>
                <span className="icon">
                    <FontAwesomeIcon icon={faSearch} />
                </span>
            </div>
        </div>
        <div className="actionButtons">
            <button className="alertButton" onClick={alertAction}>Alerta</button>
        </div>
        <MapComponent mapLat={40.4165} mapLon={-3.70256} />
    </div>
}

export default Landing;