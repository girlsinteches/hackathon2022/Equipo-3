import React from "react";

const Logout = ({...props}) => {
    window.localStorage.clear();
    window.location.href = "/";
    return <div/>
}

export default Logout;