import React, {useState} from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSearch} from "@fortawesome/free-solid-svg-icons";
import MapComponent from "../../components/map/MapComponent";
import AlertModalWatcherModal from "../../components/modal/AlertModalWatcherModal";

const AlertWatcher = ({...props}) => {
    return <div>
        <div className="searchBar">
            <div className="barContainer">
                <input type="text" placeholder={"Introduce tu destino"}/>
                <span className="icon">
                    <FontAwesomeIcon icon={faSearch} />
                </span>
            </div>
        </div>
        <div className="actionButtons">
            <button className="alertButton">Alerta</button>
        </div>
        <MapComponent mapLat={40.4165} mapLon={-3.70256}/>
        <AlertModalWatcherModal />
    </div>
}

export default AlertWatcher