import React, {useState} from "react";
import MapComponent from "../../components/map/MapComponent";
import AlertModal from "../../components/modal/AlertModal";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSearch} from "@fortawesome/free-solid-svg-icons";


const Alert = ({...props}) => {
    return <div>
        <div className="searchBar">
            <div className="barContainer">
                <input type="text" placeholder={"Introduce tu destino"}/>
                <span className="icon">
                    <FontAwesomeIcon icon={faSearch} />
                </span>
            </div>
        </div>
        <div className="actionButtons">
            <button className="alertButton">Alerta</button>
        </div>
        <MapComponent mapLat={40.4165} mapLon={-3.70256}/>
        <AlertModal />
    </div>
}

export default Alert;