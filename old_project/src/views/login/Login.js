import React, {useState} from "react";
import MapComponent from "../../components/map/MapComponent";
import LoginModal from "../../components/modal/LoginModal";


const Login = ({...props}) => {
    return <div>
        <div className="actionButtons">
            <button className="loginButton">Login</button>
            <button className="registerButton" onClick={(e) => {
                window.location.href="/register";}
            }>Registro</button>
        </div>
        <MapComponent mapLat={40.4165} mapLon={-3.70256} />
        <LoginModal />
    </div>
}

export default Login;
