import React from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import Home from './views/home/Home';
import Register from "./views/register/Register";
import Landing from "./views/landing/Landing";
import Login from "./views/login/Login";
import Alert from "./views/Alert/Alert";
import AlertWatcher from "./views/Alert/AlertWatcher";
import Logout from "./views/Logout";

function App() {
    return (
        <div className="App">
            <Router>
                <Routes>
                    <Route path={"/"} element={<Home/>}/>
                    <Route path={"/register"} exact element={<Register />}/>
                    <Route path={"/alert_list"} exact element={<Home/>}/>
                    <Route path={"/landing"} exact element={<Landing/>}/>
                    <Route path={"/login"} exact element={<Login />} />
                    <Route path={"/alert"} exact element={<Alert/>} />
                    <Route path={"/alert_watcher"} exact element={<AlertWatcher />} />
                    <Route path={"/logout"} exact element={<Logout/>}/>
                </Routes>
            </Router>
        </div>
    );
}

export default App;
