from flask import Flask, jsonify, request
from flask_cors import CORS
from config import server, graph as graph_config
from graph import Graph, GraphUtils

app = Flask(__name__)
CORS(app)
graph = Graph(**graph_config)


@app.route('/')
def home():
    return jsonify({
        'name': 'BeSafe',
        'version': '0.0.1'
    })


@app.route('/api/login', methods=['POST'])
def user_login():
    input_data = request.json
    if not input_data:
        return jsonify({
            'err': 'missing-username-email-or-password'
        }), 400
    session = graph.get_session(graph.driver)
    gutils = GraphUtils()
    login_data = gutils.login_user(session, email=input_data['email'], password=input_data['password'])
    if login_data:
        return jsonify(login_data), 201
    return jsonify({
        'err': 'user-cloud-not-been-authenticated'
    }), 401


@app.route('/api/user', methods=['GET', 'POST'])
def user_endpoint():
    if request.method == 'GET':
        ut = request.headers.get('user_token')
        if not ut:
            return jsonify({
                'err': 'non-token-provided-as-user-token-header'
            }), 400
        session = graph.get_session(graph.driver)
        gutils = GraphUtils()
        user_data = gutils.get_user_by_session_token(session, ut)
        session.close()
        if not user_data:
            return jsonify({
                'err': 'session-not-found'
            }), 404
        return jsonify(user_data)

    elif request.method == 'POST':
        # Create User
        session = graph.get_session(graph.driver)
        gutils = GraphUtils()
        input_data = request.json
        exists = gutils.check_if_user_exists(session, email=input_data['email'], nickname=input_data['nickname'])
        if exists:
            return jsonify({
                'err': 'user-exits',
                'data': {
                    'email': input_data['email'],
                    'nickname': input_data['nickname']
                }
            }), 400
        response = gutils.create_user(session, **input_data)
        print(input_data, response)
        return jsonify(response), 201


# TODO: Create Alert
@app.route('/api/alert', methods=['POST'])
def create_alert():
    token = request.headers.get('user_token')
    if not token:
        return jsonify({
            'err': 'you-have-to-log-in-to-create-alerts'
        }), 401
    input_data = request.json
    if not input_data:
        return jsonify({
            'err': 'you-must-provide-latitude-and-longitude'
        }), 400
    session = graph.get_session(graph.driver)
    gutils = GraphUtils()
    alert_info = gutils.create_alert(
        session,
        user_token=token,
        latitude=input_data['latitude'],
        longitude=input_data['longitude']
    )
    session.close()
    if not alert_info:
        return jsonify({
            'err': 'session-has-not-been-found-alert-needs-session'
        }), 401
    return jsonify({
        'status': 'alert-created',
        'alert': alert_info
    })


@app.route('/api/alerts')
def list_alerts():
    session = graph.get_session(graph.driver)
    gutils = GraphUtils()
    alerts = gutils.get_alert_list(session)
    return jsonify(alerts)


@app.route('/api/alert/<string:alert_id>', methods=['GET', 'PUT', 'DELETE'])
def manage_alert(alert_id):
    user_token = request.headers.get('user_token')
    session = graph.get_session(graph.driver)
    gutils = GraphUtils()
    if request.method == 'DELETE':
        if not user_token:
            return jsonify({
                'err': 'you-must-be-logged-to-manage-alerts'
            })
        delete_status = gutils.delete_alert_by_alert_id(session, alert_id=alert_id, user_token=user_token)
        if not delete_status:
            return jsonify({
                'err': 'you-cannot-delete-an-alert-that-does-not-belong-to-you'
            })
        return jsonify({
            'status': 'alert-deleted',
            'alert_id': alert_id
        })
    elif request.method == 'PUT':
        if not user_token:
            return jsonify({
                'err': 'you-must-be-logged-to-manage-alerts'
            })
        user_data = request.json
        if not user_data or not user_data.get('category'):
            return jsonify({
                'err': 'you-must-provide-category-data-for-updating-alerts'
            }), 400
        alert = gutils.update_alert_category_by_alert_id(session, alert_id=alert_id, user_token=user_token, category=user_data['category'])
        if not alert:
            return jsonify({
                'err': 'you-cannot-modify-an-alert-that-does-not-belong-to-you'
            }), 403
        return jsonify({
            'status': 'alert-updated',
            'alert': alert
        })
    else:
        alert = gutils.get_alert_by_alert_id(session, alert_id=alert_id)
        if not alert:
            return jsonify({
                'err': 'alert-not-found'
            }), 404
        return jsonify(alert)

# TODO: Add Emergency Contacts
# TODO: Delete Emergency Contacts
# TODO: Add Alert from News


if __name__ == '__main__':
    app.run(
        host=server['host'],
        port=server['port'],
        debug=server['debug']
    )
