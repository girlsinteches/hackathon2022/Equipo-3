import json

import requests
import random

first_names = [
    'John',
    'Jane',
    'Jake',
    'Finn',
    'Mary',
    'Tom',
    'Brie'
]

last_names = [
    'Doe',
    'Jameson',
    'Clarke',
    'Watson',
    'Parker',
    'Smith',
    'Jackson',
    'Cooper'
]

default_password = 'test123'

geopositions = [
    [40.46491625565525, -3.694128152015574],
    [40.46085161527834, -3.698318751147993],
    [40.458988895030444, -3.6825059344429985],
    [40.46937061183173, -3.6951080269439327],
    [40.4574009615802, -3.6797366848169957]
]

aggressions = [
    'aggression',
    'verbal-aggression',
    'physical-aggression',
    'sexual-aggression',
    'harassment',
    'verbal-harassment',
    'physical-harassment',
    'murder',
    'other'
]

base_url = 'http://besafe.jasonjimnzdev.es:5001'
def create_dataset_script():
    persons = []
    for x in range(20):
        person_username = ','.join([
            random.choice(first_names),
            random.choice(last_names),
            random.choice(last_names),
            str(random.randint(1, 100))
        ])
        new_person = {
            'nickname': person_username.replace(',', '_'),
            'email': '{email}@test.com'.format(email=person_username.replace(',', '_')),
            'password': default_password
        }
        persons.append(new_person)
        # Will create account
        requests.post('{base_url}/api/user'.format(base_url=base_url), data=json.dumps(new_person), headers={'Content-Type': 'application/json'}).json()
    for person in persons:
        person_login = requests.post(
            '{base_url}/api/login'.format(base_url=base_url),
            data=json.dumps({
                'email': person['email'],
                'password': person['password']
            }),
            headers={'Content-Type': 'application/json'}).json()
        token = person_login['token']
        for x in range(15):
            positions = random.choice(geopositions)
            a = requests.post(
                '{base_url}/api/alert'.format(base_url=base_url),
                data=json.dumps({
                    'latitude': positions[0] + (random.randint(1, 100) / 750),
                    'longitude': positions[1] + (random.randint(1, 100) / 1000)
                }),
                headers={
                    'Content-Type': 'application/json',
                    'user_token': token
                }).json()
            alert_id = a['alert']['alert_id']
            au = requests.put(
                '{base_url}/api/alert/{alert_id}'.format(
                    base_url=base_url,
                    alert_id=alert_id
                ),
                data=json.dumps({
                    'category': random.choice(aggressions)
                }),
                headers={
                    'Content-Type': 'application/json',
                    'user_token': token
                }).json()
            print("created alert")
    return True
