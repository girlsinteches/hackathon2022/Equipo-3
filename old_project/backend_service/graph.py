from neo4j import GraphDatabase, basic_auth, Driver, Session, Result
from config import secret
import uuid
import datetime
import hashlib


def parse_password(plain_password: str, secret: str) -> str:
    modified_password = hashlib.sha256()
    modified_password.update(secret.encode('utf-8'))
    modified_password.update(plain_password.encode('utf-8'))
    return modified_password.hexdigest()


class Graph(object):
    driver = None

    def __init__(self, host: str, port: int, user: str, password: str):
        self.driver = self.get_driver(
            host=host,
            port=port,
            user=user,
            password=password
        )

    @classmethod
    def get_driver(cls, host: str, port: int, user: str, password: str) -> Driver:
        return GraphDatabase.driver(
            'bolt://{host}:{port}'.format(
                host=host,
                port=port
            ),
            auth=basic_auth(
                user=user,
                password=password
            )
        )

    @classmethod
    def get_session(cls, driver: Driver) -> Session:
        return driver.session()


class GraphOperations(object):
    queries = {}

    def __init__(self, graph_queries: dict = None):
        if graph_queries:
            self.queries = graph_queries

    def run_query(self, session: Session, query_key: str, query_data: dict = None) -> Result | None:
        if not self.queries.get(query_key):
            return None

        if query_data:
            return session.run(self.queries.get(query_key).format(query_data))
        else:
            return session.run(self.queries.get(query_key))


class GraphUtils(object):
    graph_ops = GraphOperations()

    @classmethod
    def create_user(cls, session: Session, email: str, nickname: str, password: str) -> dict | None:
        query = """MERGE (u:User {{
            uuid: "{uuid}",
            email: "{email}",
            nickname: "{nickname}",
            password: "{password}",
            created_at: "{created_at}",
            updated_at: "{created_at}"
        }}) RETURN 
            u.uuid AS uuid, 
            u.email AS email,
            u.nickname AS nickname,
            u.created_at AS created_at,
            u.updated_at AS updated_at""".format(
            email=email,
            nickname=nickname,
            password=password,
            created_at=datetime.datetime.utcnow().isoformat(),
            uuid=uuid.uuid4().hex
        )
        print(query)
        results = session.run(query)
        records = [{
            "uuid": r['uuid'],
            "email": r['email'],
            "nickname": r['nickname'],
            "created_at": r['created_at'],
            "updated_at": r['updated_at']
        } for r in results.data()]
        if len(records):
            return records[0]
        else:
            return None

    @classmethod
    def check_if_user_exists(cls, session: Session, email: str, nickname: str) -> bool:
        query = """MATCH (u:User) 
            WHERE u.email = "{email}" 
            OR u.nickname = "{nickname}" 
            RETURN u.uuid AS uuid""".format(email=email, nickname=nickname)
        results = session.run(query)
        records = [r for r in results.data()]
        return len(records) != 0

    @classmethod
    def login_user(cls, session: Session, email: str, password: str) -> dict | None:
        query = """MATCH (u:User) 
            WHERE u.email = "{email}" 
            AND u.password = "{password}" 
            RETURN u.uuid AS uuid """.format(email=email, password=password)
        result = session.run(query)
        records = [r['uuid'] for r in result.data()]
        if len(records) == 0:
            return None
        user_uuid = records[0]
        query_2 = """MERGE (s:UserSession {{
                token: "{session_token}",
                created_at: "{created_at}",
                valid_until: "{valid_until}"
            }})
            WITH s
            MATCH (u:User{{uuid: "{user_uuid}"}})
            WITH u, s
            CREATE (u)<-[:SESSION_OF]-(s)
            RETURN s.token AS token, 
            s.created_at AS created_at, 
            s.valid_until AS valid_until,
            u.uuid AS user_uuid""".format(
            session_token=uuid.uuid4().hex,
            created_at=datetime.datetime.utcnow().isoformat(),
            valid_until=(datetime.datetime.utcnow() + datetime.timedelta(days=2)).isoformat(),
            user_uuid=user_uuid
        )
        result_2 = session.run(query_2)
        records_2 = [{
            'token': r['token'],
            'created_at': r['created_at'],
            'valid_until': r['valid_until'],
            'user_uuid': r['user_uuid']
        } for r in result_2.data()]
        if len(records_2) == 0:
            return None
        return records_2[0]

    @classmethod
    def get_user_by_session_token(cls, session: Session, token: str) -> dict | None:
        query = """MATCH (u:User)<-[:SESSION_OF]-(s:UserSession) 
            WHERE s.token = "{session_token}" 
            RETURN u.nickname AS nickname, 
            u.email AS email, 
            u.uuid AS user_uuid, 
            s.created_at AS session_created, 
            s.valid_until AS session_valid_until""".format(session_token=token)
        results = session.run(query)
        records = [{
            'email': r['email'],
            'uuid': r['user_uuid'],
            'session_created_at': r['session_created'],
            'session_valid_until': r['session_valid_until']
        } for r in results.data()]
        if len(records) == 0:
            return None
        return records[0]

    @classmethod
    def create_alert(cls, session: Session, user_token: str, latitude: str, longitude: str) -> dict | None:
        query = """MATCH (u:User)<-[:SESSION_OF]-(s:UserSession {{token: "{user_token}"}})
            WITH u, s
            CREATE (a:Alert {{
                alert_id: "{alert_id}",
                created_at: "{created_at}",
                updated_at: "{created_at}",
                lat: "{latitude}",
                lon: "{longitude}"
            }})
            WITH u, s, a
            CREATE (a)-[:ALERT_CREATED_BY]->(u)
            RETURN a.alert_id as alert_id
            """.format(
            alert_id=uuid.uuid4().hex,
            created_at=datetime.datetime.utcnow().isoformat(),
            latitude=latitude,
            longitude=longitude,
            user_token=user_token
        )
        result = session.run(query)
        records = [{
            'alert_id': r['alert_id']
        } for r in result.data()]
        if len(records) == 0:
            return None
        return records[0]

    @classmethod
    def get_alert_list(cls, session: Session, **kwargs) -> list:
        limit = 300
        if kwargs.get('limit'):
            if type(kwargs.get('limit')) == int:
                limit = kwargs.get('limit')
        query = """MATCH (a:Alert) 
        RETURN a.alert_id AS alert_id, 
        a.created_at AS created_at, 
        a.updated_at AS updated_at,
        a.category AS category,
        a.lat AS latitude,
        a.lon AS longitude
        ORDER BY datetime(a.created_at) DESC LIMIT {limit_query}""".format(
            limit_query=limit
        )
        result = session.run(query)
        records = [{
            'alert_id': r['alert_id'],
            'created_at': r['created_at'],
            'updated_at': r['updated_at'],
            'category': r['category'],
            'latitude': r['latitude'],
            'longitude': r['longitude']
        } for r in result.data()]
        return records

    @classmethod
    def get_alert_by_alert_id(cls, session: Session, alert_id: str) -> dict | None:
        query = """MATCH (a:Alert) 
            WHERE a.alert_id = "{alert_id}" 
            RETURN a.alert_id AS alert_id, 
            a.created_at AS created_at, 
            a.updated_at AS updated_at, 
            a.category AS category, 
            a.lat AS latitude, 
            a.lon AS longitude""".format(
            alert_id=alert_id
        )
        result = session.run(query)
        records = [{
            'alert_id': r['alert_id'],
            'created_at': r['created_at'],
            'updated_at': r['updated_at'],
            'category': r['category'],
            'latitude': r['latitude'],
            'longitude': r['longitude']
        } for r in result.data()]
        if len(records) == 0:
            return None
        return records[0]

    @classmethod
    def update_alert_category_by_alert_id(cls, session: Session, alert_id: str, user_token: str, category: str):
        query = """MATCH (a:Alert)-[:ALERT_CREATED_BY]->(u:User)<-[:SESSION_OF]-(s:UserSession)
                    WHERE a.alert_id = "{alert_id}" 
                    AND s.token = "{user_token}"
                    SET a.category = "{category}"
                    RETURN a.alert_id AS alert_id, 
                    a.created_at AS created_at, 
                    a.updated_at AS updated_at, 
                    a.category AS category, 
                    a.lat AS latitude, 
                    a.lon AS longitude""".format(alert_id=alert_id, category=category, user_token=user_token)
        result = session.run(query)
        records = [{
            'alert_id': r['alert_id'],
            'created_at': r['created_at'],
            'updated_at': r['updated_at'],
            'category': r['category'],
            'latitude': r['latitude'],
            'longitude': r['longitude']
        } for r in result.data()]
        if len(records) == 0:
            return None
        return records[0]

    @classmethod
    def delete_alert_by_alert_id(cls, session: Session, alert_id: str, user_token: str) -> bool:
        query = """MATCH (a:Alert)-[r:ALERT_CREATED_BY]->(u:User)<-[:SESSION_OF]-(s:UserSession)
                    WHERE a.alert_id = "{alert_id}" 
                    AND s.token = "{user_token}"
                    DELETE r, a""".format(alert_id=alert_id, user_token=user_token)
        query_2 = """MATCH (a:Alert) WHERE a.alert_id = "{alert_id}" RETURN a""".format(alert_id=alert_id)
        try:
            session.run(query)
            result = session.run(query_2)
            records = [r for r in result.data()]
            if len(records) == 0:
                return True
            else:
                return False
        except Exception as e:
            print("Error deleting: ")
            print(e)
            return False
