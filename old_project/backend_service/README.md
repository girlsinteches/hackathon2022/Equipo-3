# BeSafe Backend

Servicio de Backend para BeSafe

## Tecnologías
- Python 3.6+
- Neo4J 3.1+

## Dependencias
- flask
- flask_cors
- neo4j_driver
- requests

## Datos de prueba

Se ha añadido una función de python en
dataset_generator.py añade 20 usuarios y
300 alertas posicionadas en Madrid con 
distintas IDs 

## Instalación

Instalamos las dependencias listadas en 
el fichero requirements.txt
```shell
pip install -r requiements.txt
```

Creamos un fichero config.py a partir del
fichero config.py.dist con la configuración
que queramos ponerle, ya que es estrictamente
necesario para la lógica del servicio.

No hay que olvidar que también es necesario
tener una instancia activa de Neo4J puedes utilizar un
[contenedor docker preparado aquí](https://hub.docker.com/r/jasonjimnz/neo4j-community)
```shell
docker run -p 7687:7687 -p 7474:7474 -d -t jasonjimnz/neo4j-community
```

Una vez activa la base de datos de grafos podemos
lanzar el servicio desde Python y expondrá el host
que hayamos definido en el config y en el puerto
que le hayamos definido también
```shell
python api.js.py
```

## Endpoints
- /api/login
  - Método: POST
  - Content-Type: application/json
  - Datos a enviar: 
    - email
    - password
  - Respuesta: JSON con datos del usuario y token de sesión
  - Respuesta si falla: Código de error con el mensaje
- /api/user
  - Métodos: GET|POST
  - Content-Type: application/json
  - Datos a enviar por cabecera por GET:
    - user_token: el token de sesión del usuario
  - Datos a enviar por POST:
    - nickname
    - email
    - password
  - Respuesta: JSON con los datos del usuario 
  creado o el usuario de consulta
  - Respuesta si falla: Código de error con el mensaje
- /api/alert
  - Método: POST
  - Content-Type: application/json
  - Datos a enviar por cabecera por GET:
    - user_token: el token de sesión del usuario
  - Datos a enviar por POST:
    - latitud
    - longitud
  - Respuesta: JSON con los datos de alerta
- /api/alerts
  - Método: GET
  - Respuesta: Array de Alertas con sus datos
- /api/alert/ALERT_ID
  - Métodos: GET, PUT, DELETE
  - Content-Type: application/json
  - Datos a enviar por cabecera:
    - user_token: el token de sesión del usuario
  - Datos a enviar por PUT:
    - category: categoría de la agresión
  - Respuesta:
    - GET: datos de la alerta
    - PUT: datos de la alerta actualizados
    - DELETE: Confirmación de borrado de la alerta
  - NOTAS:
    - Solo el usuario que ha creado la alerta puede
    editarla o borrarla, devolverá un error en caso
    de que se intente borrar una que no le pertenece