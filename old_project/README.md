# BeSafe

Para viajar con seguridad en el día a día

## Equipo Hacking For Humanity (Equipo 3)
- Jason Jiménez Cruz
- Sandra Lescano Vizcardo

## Tecnologías
- React
- Python (backend)

## Requisitos
- NodeJs
- Apache2
- Python3
- Neo4J
- Activar módulo rewrite de Apache

La documentación del backend la pueden encontrar 
[aquí](backend_service/README.md)

## Instalación

Instalamos NodeJS NPM y el servidor web Apache
```shell
sudo apt-get install nodejs npm apache2
```

Activamos el módulo rewrite de Apache y reiniciamos el servicio
```shell
sudo a2enmod rewrite && sudo service apache2 restart
```

Desde la raíz del repositorio, instalamos las dependencias, 
a partir de ahí podemos o bien lanzar la aplicación en desarrollo
o crear una build
```shell
npm install
# Para lanzar entorno de desarrollo
npm start
# Para construir una build
npm run build
```

El fichero deploy_linux.sh permite el despliegue por comando en un 
servidor Apache, no obstante se puede utilizar el paquete serve de
npm para servir la build creada 

```shell
# Posiblemente necesites permisos para instalar paquetes globales
npm install -g serve
# Te crea un servidor donde sirve nuestra aplicación de React
serve -s build
```